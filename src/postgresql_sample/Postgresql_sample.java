package postgresql_sample;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author codeOlam
 */
public class Postgresql_sample {
    
    private Connection con;
    private Statement stmt;
    private PreparedStatement ps;
    
    
    private final String url = "jdbc:postgresql://localhost:5432/pg_sample_db";
    private final String user = "yourUsername";     //Replace with your postgres credentials
    private final String password = "YourPswd";     //Replace with your postgres credentials
    
    public Connection dbConnection() {
        
        try {
            
            con = DriverManager.getConnection(url, user, password);
            
            System.out.println("Connection Successfull!");
            
            return con;
        } catch (Exception ex) {
            
            System.out.println("Connection Failed!");
            return null;
        }
    }
    
    public void create_table() {
        
        try {
            dbConnection();
            
            stmt = con.createStatement();
            
            String sql = "CREATE TABLE IF NOT EXISTS staff"+
                         "(id serial primary key not null,"
                       + " first_name varchar(50) not null,"
                       + " last_name varchar(50) not null,  "
                       + " time_stamp timestamp)";
            
            stmt.executeUpdate(sql);
            stmt.close();
            
            
            System.out.println("Table created!");
        } catch (SQLException ex) {
            Logger.getLogger(Postgresql_sample.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Not successful in creating the table");
        }
    }
    
    public void insertData(){
        try {

            create_table();
            
            String firstName = "Frank";
            String lastName = "Uchenna";
            LocalDateTime time_stp = LocalDateTime.now(); 
            
            String sql = "INSERT INTO staff(first_name, last_name, time_stamp ) VALUES(?, ?, ?)";
         
            ps = con.prepareStatement(sql);
            

            ps.setString(1, firstName);
            ps.setString(2, lastName);
            ps.setObject(3, time_stp);
            
            ps.executeUpdate();
            
            con.close();
            
            System.out.println("\nData Inserted");
            
        } catch (SQLException ex) {
            Logger.getLogger(Postgresql_sample.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("\nCould not insert data");
        }
    }
    
    public void print_db(){
        try {
            dbConnection();
            String query = "SELECT * FROM staff";
            
            ps = con.prepareStatement(query);
            
            ResultSet rs = ps.executeQuery();
            
            
            while (rs.next()){
                System.out.print(rs.getInt(1));
                System.out.print(":\t");
                System.out.print(rs.getString(2));
                System.out.print("\t");
                System.out.print(rs.getString(3));                
                System.out.print("\t");
                System.out.println(rs.getString(4));
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Postgresql_sample.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Problem printing table");
        }
    }


    public static void main(String[] args) {
        // TODO code application logic here
        Postgresql_sample db = new Postgresql_sample();
        
        db.insertData();
        db.print_db();
        
    }
    
}
