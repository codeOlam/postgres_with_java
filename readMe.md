## Simple Postgresql implementation on JAVA.

![photo](pgjava.png?raw=true "pic")




## About
This is a simple java application. This application implements postgresql using jdbc API, with the following features 

- Create table 
- inserts data into table
- retrieves data from the database table
- Datetime handling with JDBC driver


## Technology and Requirements
1. Java
2. Postgresql
3. Netbeans




## Installations
1. [NetBeans](https://netbeans.org/community/releases/82/install.html)
2. [PostgreSql](http://www.techken.in/how-to/install-postgresql-10-windows-10-linux/)

## Resources
- [TutorialPoint](https://www.tutorialspoint.com/postgresql/postgresql_java.htm)
- [DateTime](https://jdbc.postgresql.org/documentation/head/8-date-time.html)

## Other Resources
- [SceneBuilder](https://gluonhq.com/products/scene-builder/)
- [java.time package](https://docs.oracle.com/javase/8/docs/api/java/time/package-summary.html)
